package com.tutorial;

class Buku{
    String judul;
    String penulis;

    Buku(String judul,String penulis){
       this.judul = judul;
       this.penulis = penulis;

    }

    void Display(){
        System.out.println("Judul   : "+this.judul);
        System.out.println("Penulis : "+this.penulis);
    }
}
public class refObject {
    public static void main(String[] args) {
      Buku buku1 = new Buku("Tendangan Madun","Tzandra");

      buku1.Display(); 

      //menampilkan adress
      String adressbuku1 = Integer.toHexString(System.identityHashCode(buku1));
      System.out.println(adressbuku1);

      //assignment object
      Buku buku2 = new Buku("Tendangan Madun","Tzandra");

      buku2.Display();
      String adressbuku2 = Integer.toHexString(System.identityHashCode(buku2));
      System.out.println(adressbuku2);

      //karena buku1 = buku2 adress sama
      buku1.judul =" Madun Shoot";
      buku1.Display();
      buku2.Display();

        //memasukkan ke dalam method
        fungsi1(buku2);
       
    }

    public static void fungsi1(Buku dataBuku){
    String adressdatabuku2 = Integer.toHexString(System.identityHashCode(dataBuku));
    System.out.println(adressdatabuku2);
  
    }
}
  