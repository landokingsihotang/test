package com.tutorial;

import java.io.IOException;
import java.math.BigDecimal;

 class TradingApp {
  public static void main(String[] args) throws IOException {
    TradingPlatform platform = new TradingPlatform();
    BigDecimal price = platform.getPrice("AAPL");
    System.out.println("Current price of AAPL: " + price);

    BigDecimal quantity = new BigDecimal(100);
    Order order = new Order("AAPL", OrderType.BUY, quantity);
    platform.executeOrder(order);
    System.out.println("Order executed: " + order);
  }
}

enum OrderType {
  BUY, SELL
}

class Order {
  private String symbol;
  private OrderType type;
  private BigDecimal quantity;

  public Order(String symbol, OrderType type, BigDecimal quantity) {
    this.symbol = symbol;
    this.type = type;
    this.quantity = quantity;
  }

  public String getSymbol() {
    return symbol;
  }

  public OrderType getType() {
    return type;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  @Override
  public String toString() {
    return symbol + " " + type + " " + quantity;
  }
}

class TradingPlatform {
  public BigDecimal getPrice(String symbol) throws IOException {
    // Connect to the trading platform's API to retrieve the current price of the given symbol
    // Example implementation: return new BigDecimal(Math.random() * 100);
    return new BigDecimal(Math.random() * 100);
  }

  public void executeOrder(Order order) throws IOException {
    // Connect to the trading platform's API to execute the given order
    System.out.println("Executing order: " + order);
  }
}
