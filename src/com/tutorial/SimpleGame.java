package com.tutorial;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class SimpleGame extends JPanel {
  private static final long serialVersionUID = 1L;
  private final int SIZE = 500;
  private final int DOT_SIZE = 10;
  private final int ALL_DOTS = (SIZE * SIZE) / (DOT_SIZE * DOT_SIZE);
  private int dots;
  private int apple_x;
  private int apple_y;
  private ArrayList<Point> snake;
  private Random random;

  public SimpleGame() {
    setPreferredSize(new Dimension(SIZE, SIZE));
    setBackground(Color.WHITE);
    setFocusable(true);
    addKeyListener(new Keyboard());
    initGame();
  }

  private void initGame() {
    dots = 3;
    random = new Random();
    snake = new ArrayList<>();
    for (int i = 0; i < dots; i++) {
      snake.add(new Point(150 - i * DOT_SIZE, 150));
    }
    locateApple();
  }

  private void locateApple() {
    int r = random.nextInt(SIZE / DOT_SIZE);
    apple_x = r * DOT_SIZE;
    r = random.nextInt(SIZE / DOT_SIZE);
    apple_y = r * DOT_SIZE;
  }

  private void checkApple() {
    if ((snake.get(0).x == apple_x) && (snake.get(0).y == apple_y)) {
      dots++;
      snake.add(new Point(apple_x, apple_y));
      locateApple();
    }
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(Color.RED);
    g.fillOval(apple_x, apple_y, DOT_SIZE, DOT_SIZE);
    for (Point point : snake) {
      g.setColor(Color.BLACK);
      g.fillRect(point.x, point.y, DOT_SIZE, DOT_SIZE);
    }
  }

  private class Keyboard implements KeyListener {
    @Override
    public void keyPressed(KeyEvent event) {
      int key = event.getKeyCode();
      if ((key == KeyEvent.VK_LEFT) && (dots > 0)) {
        moveLeft();
      }
      if ((key == KeyEvent.VK_RIGHT) && (dots > 0)) {
        moveRight();
      }
      if ((key == KeyEvent.VK_UP) && (dots > 0)) {
        moveUp();
      }
      if ((key == KeyEvent.VK_DOWN) && (dots > 0)) {
        moveDown();
    }
    }
    @Override
public void keyReleased(KeyEvent e) {}

@Override
public void keyTyped(KeyEvent e) {}
}

private void moveLeft() {
for (int i = snake.size() - 1; i > 0; i--) {
snake.set(i, new Point(snake.get(i - 1)));
}
snake.set(0, new Point(snake.get(0).x - DOT_SIZE, snake.get(0).y));
}

private void moveRight() {
for (int i = snake.size() - 1; i > 0; i--) {
snake.set(i, new Point(snake.get(i - 1)));
}
snake.set(0, new Point(snake.get(0).x + DOT_SIZE, snake.get(0).y));
}

private void moveUp() {
for (int i = snake.size() - 1; i > 0; i--) {
snake.set(i, new Point(snake.get(i - 1)));
}
snake.set(0, new Point(snake.get(0).x, snake.get(0).y - DOT_SIZE));
}

private void moveDown() {
for (int i = snake.size() - 1; i > 0; i--) {
snake.set(i, new Point(snake.get(i - 1)));
}
snake.set(0, new Point(snake.get(0).x, snake.get(0).y + DOT_SIZE));
}

public static void main(String[] args) {
JFrame frame = new JFrame("Simple Snake Game");
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
SimpleGame game = new SimpleGame();
frame.add(game);
frame.pack();
frame.setLocationRelativeTo(null);
frame.setVisible(true);
while (true) {
game.checkApple();
game.repaint();
try {
Thread.sleep(100);
} catch (InterruptedException e) {
e.printStackTrace();
}
}
}
}
