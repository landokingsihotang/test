package com.tutorial;


class Hero{
    String name;

    public void display(){
        System.out.println("Name : "+this.name);
    }
}

class heroStrength extends Hero{
    //hampa 
}

class heroIntel extends Hero{

}
public class Inheritance {
    public static void main(String[] args) {
         Hero hero1 = new Hero();
         hero1.name = "ucup";
         hero1.display();

         heroStrength heroStrength1 = new heroStrength();
         heroStrength1.name = "ucap";
         heroStrength1.display();

         heroIntel heroIntel = new heroIntel();
         heroIntel.name = "ucop";
         heroIntel.display();
    }
}
