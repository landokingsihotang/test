package com.tutorial;

class Layer{
    String name;
    public int cls;

    Layer(String name,int cls){
        this.name = name;
        this.cls = cls;
    }
   void layerDisplay(){
        System.out.println(this.name);
    }
}

public class PrivStat {
 public static void main(String[] args) {
    
    //default
    Layer player1 = new Layer("Mario",0);
    player1.layerDisplay();
    player1.name = "Maria";
    player1.layerDisplay();

    //public
    
 }                 
}
